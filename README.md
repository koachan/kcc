kcc - A simple compiler in Kotlin
---
This is basically just a simple compiler for me to learn about compiler theory.
The codebase is neither good nor optimized :p

# Wishlist
## Language
- Implement a big enough subset of A Serious Programming Language[^proglang] that it can be used for Serious Programs™.
- Learn some compiler stuff along the way, including how to make it retargetable.
- Also learn some optimizations (esp. vectorizations).

## Target platforms
- Linux on AMD64 (aka x86_64), with SSE and SSE2 vector instruction set.
- Linux on PowerPC64 big endian, with AltiVec vector instruction set using ELFv2 ABI.

# References
Those are the books & webpages I read to learn about abstract concepts:

- Modern Compiler Design (1st ed) by Grune & Bal & Jacobs & Langendoen
- ScottW's "[Understanding Parser Combinator](https://fsharpforfunandprofit.com/posts/understanding-parser-combinators/)" series of blogposts
- https://github.com/zakirullin/tiny-compiler
- https://c9x.me/compile/bib/

[^proglang]: Currently thinking of doing C89, but I might change plans.
