package kcc.lang

import kotlin.test.Test

internal class TinyLangParserTest {
    @Test
    fun testBasicExprSuccess() {
        val tests = hashMapOf(
            Pair("true;", Program(Scope(listOf(BooleanLit(true))))),
            Pair("false;", Program(Scope(listOf(BooleanLit(false))))),
            Pair("1;", Program(Scope(listOf(IntegerLit(1))))),
            Pair("i=2;", Program(Scope(listOf(Assignment(Identifier("i"), IntegerLit(2)))))),
            Pair("b=false;", Program(Scope(listOf(Assignment(Identifier("b"), BooleanLit(false)))))),
            Pair("3+4;", Program(Scope(listOf(Expression(BinaryOperators.ADD, IntegerLit(3), IntegerLit(4)))))),
            Pair("5==6;", Program(Scope(listOf(Expression(BinaryOperators.EQ, IntegerLit(5), IntegerLit(6)))))),
            Pair("true&&false;", Program(Scope(listOf(Expression(BinaryOperators.AND, BooleanLit(true), BooleanLit(false)))))),
            Pair("!true;", Program(Scope(listOf(BooleanNegate(BooleanLit(true)))))),
            Pair("-7;", Program(Scope(listOf(IntegerNegate(IntegerLit(7))))))
        )

        for (t in tests) {
            val res = parseLanguage(listOf(t.key))
            assert(res.isSuccess && res.getOrNull()!! == t.value) { t.key }
        }
    }
}
