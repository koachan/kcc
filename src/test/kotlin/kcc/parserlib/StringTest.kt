package kcc.parserlib

import kotlin.test.Test

internal class StringParserTest {
    private val testSP = StringParser("test")

    @Test
    fun emptyInput() {
        val res = testSP.parse("")
        assert(res is ParseError)
    }

    @Test
    fun correctInput() {
        val res = testSP.parse("test")
        assert(res is ParseSuccess)
    }

    @Test
    fun wrongInput() {
        val res = testSP.parse("wrong")
        assert(res is ParseError)
    }
}

internal class AnyOfStringTest {
    private val testAOS = AnyOfString("test1", "test2", "test3")

    @Test
    fun emptyInput() {
        val res = testAOS.parse("")
        assert(res is ParseError)
    }

    @Test
    fun correctInput() {
        val res = listOf(testAOS.parse("test1"),
            testAOS.parse("test2"),
            testAOS.parse("test3"))
        assert(res.fold(true) { a, it -> a && (it is ParseSuccess) })
    }

    @Test
    fun wrongInput() {
        val res = testAOS.parse("test4")
        assert(res is ParseError)
    }
}

internal class CharParserTest {
    private val testCP = CharParser('a')

    @Test
    fun emptyInput() {
        val res = testCP.parse("")
        assert(res is ParseError)
    }

    @Test
    fun correctInput() {
        val res = testCP.parse("a")
        assert(res is ParseSuccess)
    }

    @Test
    fun wrongInput() {
        val res = testCP.parse("b")
        assert(res is ParseError)
    }
}

internal class CharacterRangeParserTest {
    private val testCRP = CharacterRangeParser('a'..'z')

    @Test
    fun insideRange() {
        val res = testCRP.parse("c")
        assert(res is ParseSuccess)
    }

    @Test
    fun outsideRange() {
        val res = testCRP.parse("C")
        assert(res is ParseError)
    }
}

internal class AnyOfCharTest {
    private val testAOC = AnyOfChar('a', 'c', 'e')

    @Test
    fun emptyInput() {
        val res = testAOC.parse("")
        assert(res is ParseError)
    }

    @Test
    fun correctInput() {
        val res = listOf(testAOC.parse("a"),
            testAOC.parse("c"),
            testAOC.parse("e"))
        assert(res.fold(true) { a, it -> a && (it is ParseSuccess) })
    }

    @Test
    fun wrongInput() {
        val res = testAOC.parse("b")
        assert(res is ParseError)
    }
}
