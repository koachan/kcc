package kcc.parserlib

import kotlin.test.Test

internal class AndThenTest {
    private val successParser1 = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(1, input)
        }
    }

    private val successParser2 = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(2, input)
        }
    }

    private val successParser = successParser1

    private val failureParser = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseError(0, 0) { "Error" }
        }
    }

    @Test
    fun testAndThen() {
        val res1 = (successParser1 AndThen successParser2).parse("")
        val res2 = (successParser AndThen failureParser).parse("")
        val res3 = (failureParser AndThen successParser).parse("")
        val res4 = (failureParser AndThen failureParser).parse("")

        assert(res1 is ParseSuccess && (res1.recognized == Pair(1, 2)))
        assert(res2 is ParseError)
        assert(res3 is ParseError)
        assert(res4 is ParseError)
    }
}

internal class OrElseTest {
    private val successParser1 = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(1, input)
        }
    }

    private val successParser2 = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(2, input)
        }
    }

    private val successParser = successParser1

    private val failureParser = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseError(0, 0) { "Error" }
        }
    }

    @Test
    fun testOrElse() {
        val res1 = (successParser1 OrElse successParser2).parse("")
        val res2 = (successParser OrElse failureParser).parse("")
        val res3 = (failureParser OrElse successParser).parse("")
        val res4 = (failureParser OrElse failureParser).parse("")

        assert(res1 is ParseSuccess && res1.recognized == 1)
        assert(res2 is ParseSuccess && res2.recognized == 1)
        assert(res3 is ParseSuccess && res3.recognized == 1)
        assert(res4 is ParseError)
    }
}

internal class KeepLeftTest {
    private val successParser1 = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(1, input)
        }
    }

    private val successParser2 = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(2, input)
        }
    }

    @Test
    fun testKeepLeft() {
        val res = (successParser1 KeepLeft successParser2).parse("")
        assert(res is ParseSuccess && res.recognized == 1)
    }
}

internal class KeepRightTest {
    private val successParser1 = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(1, input)
        }
    }

    private val successParser2 = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(2, input)
        }
    }

    @Test
    fun testKeepLeft() {
        val res = (successParser1 KeepRight successParser2).parse("")
        assert(res is ParseSuccess && res.recognized == 2)
    }
}
