package kcc.parserlib

import kotlin.test.Test

internal class MapTest {
    private val successParser = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseSuccess(1, input)
        }
    }

    private val failureParser = object : Parser<Int>() {
        override fun parseImpl(input: ParseState): ParseResult<Int> {
            return ParseError(0, 0) { "Error" }
        }
    }

    private val transform = { a: Int -> a + 1 }

    @Test
    fun mappedSuccess() {
        val res = successParser.parse("")
        val mappedRes = successParser.map(transform).parse("")
        assert(mappedRes is ParseSuccess)
        assert((mappedRes as ParseSuccess).recognized == transform((res as ParseSuccess).recognized))
    }

    @Test
    fun mappedFailure() {
        val res = failureParser.map(transform).parse("")
        assert(res is ParseError)
    }
}
