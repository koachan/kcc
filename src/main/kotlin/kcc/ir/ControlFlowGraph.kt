package kcc.ir

// Design follows Appel's Modern Compiler Implementation in Java, 2nd ed.
// This is specalized only for the middle IR.

data class BasicBlock(val label: IROperation.Label, val ops: MutableList<IROperation> = ArrayList()) {
    init { ops.add(label) }

    override fun toString(): String {
        return ops.joinToString("\n")
    }

    // Two basic blocks are considered to be the same if they have the same label.
    // Kinda hacky but it's sufficient for current purposes.
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as BasicBlock
        return label == other.label
    }

    override fun hashCode() = label.hashCode()
}

class ControlFlowGraph {
    private val predecessorSets = HashMap<BasicBlock, HashSet<BasicBlock>>()
    private val successorSets = HashMap<BasicBlock, HashSet<BasicBlock>>()

    // Needs to preserve order; the first node is our entrypoint
    // Using LinkedHashMap as a hack to be able to look up quickly by name
    private val nodes = LinkedHashSet<BasicBlock>()

    fun nodes(): Set<BasicBlock> = nodes

    fun addBlock(bb: BasicBlock) = nodes.add(bb)

    fun addFlow(from: BasicBlock, to: BasicBlock) {
        addBlock(from); addBlock(to)

        when (val p = predecessorSets[to]) {
            null -> predecessorSets[to] = hashSetOf(from)
            else -> p.add(from)
        }

        when (val s = successorSets[from]) {
            null -> successorSets[from] = hashSetOf(to)
            else -> s.add(to)
        }
    }

    fun rmFlow(from: BasicBlock, to: BasicBlock) {
        when (val p = predecessorSets[to]) {
            null -> {}
            else -> p.remove(from)
        }

        when (val s = successorSets[from]) {
            null -> {}
            else -> s.remove(to)
        }
    }

    fun predecessorsOf(bb: BasicBlock): Set<BasicBlock> {
        return when (val p = predecessorSets[bb]) {
            null -> HashSet()
            else -> p
        }
    }

    fun successorsOf(bb: BasicBlock): Set<BasicBlock> {
        return when (val s = successorSets[bb]) {
            null -> HashSet()
            else -> s
        }
    }

    fun adjacentsOf(bb: BasicBlock) = predecessorsOf(bb) + successorsOf(bb)

    override fun toString() = nodes().joinToString(separator = "\n\n", postfix = "\n") { it.toString() }
}

data class CFGProgram(val graph: ControlFlowGraph, val startBlock: BasicBlock) {
    init {
        if (startBlock !in graph.nodes()) throw IllegalArgumentException("Starting block must be inside the graph")
    }

    override fun toString() = graph.toString()

    // Remove Phi ops and get this CFGProgram out of SSA form
    // Running it on programs that's already in non-SSA form should produce no effects.
    fun convertOutOfSSA() {
        // TODO: Replace this with a more robust version that can withstand TSSA and cycles
        val queue: ArrayDeque<BasicBlock> = ArrayDeque()
        val seen: HashSet<BasicBlock> = HashSet()
        queue += startBlock
        while (queue.isNotEmpty()) {
            val b = queue.removeFirst()
            if (b in seen) continue else seen += b

            val succs = graph.successorsOf(b)
            queue += succs

            // Add a IROperation.Copy op below the referenced variable
            // then replace the phi op with a copy to itself. The current method needs it
            // to look up to all the previous predecessors, which will be slow (O(n^2)),
            // but is enough for now.

            // vX = a;
            // ...
            // vY = b;
            // ...
            // vZ = phi(vX, vY);
            // Becomes
            // vX = a;
            // vZ = vX;
            // ...
            // vY = b;
            // vZ = vY;
            // ...
            // vZ = vZ;

            for ((i, op) in b.ops.withIndex()) {
                when (op) {
                    is IROperation.Phi -> {
                        val predQueue: ArrayDeque<BasicBlock> = ArrayDeque()
                        val predSeen: HashSet<BasicBlock> = HashSet()

                        predQueue += graph.predecessorsOf(b)
                        while(predQueue.isNotEmpty()) {
                            val p = predQueue.removeFirst()
                            if (p in predSeen) continue else predSeen += p

                            val preds = graph.predecessorsOf(p)
                            predQueue += preds

                            val raIndex = p.ops.withIndex().filter { (op.ra in it.value.defs()) }.map { it.index }
                            for (j in raIndex) {
                                p.ops.add(j + 1, IROperation.Copy(op.rd, op.ra))
                            }

                            val rbIndex = p.ops.withIndex().filter { (op.rb in it.value.defs()) }.map { it.index }
                            for (j in rbIndex) {
                                p.ops.add(j + 1, IROperation.Copy(op.rd, op.rb))
                            }
                        }

                        b.ops[i] = IROperation.Copy(op.rd, op.rd)
                    }
                    else -> { /* Let it pass through */ }
                }
            }
        }
    }
}
