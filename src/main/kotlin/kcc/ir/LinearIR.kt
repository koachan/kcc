package kcc.ir

data class LinearProgram<T> (val ops: List<T>) {
    companion object {
        operator fun invoke(cfgProgram: CFGProgram): LinearProgram<IROperation> {
            val ops = ArrayList<IROperation>()

            for (b in cfgProgram.graph.nodes()) {
                ops.addAll(b.ops)
            }

            return LinearProgram(ops)
        }
    }

    override fun toString(): String = ops.joinToString(separator = "\n", postfix = "\n")
}
