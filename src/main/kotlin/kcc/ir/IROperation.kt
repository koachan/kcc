package kcc.ir

data class Var(val num: Int) {
    override fun toString() = "v$num"
}

sealed class IROperation {
    abstract fun defs(): Set<Var>
    abstract fun uses(): Set<Var>

    data class ImmBoolean(val rd: Var, val imm: Boolean) : IROperation() {
        override fun toString() = "$rd = boolean($imm);"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf()
    }

    data class ImmI64(val rd: Var, val imm: Long) : IROperation() {
        override fun toString() = "$rd = i64($imm);"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf()
    }

    data class Copy(val rd: Var, val rs: Var) : IROperation() {
        override fun toString() = "$rd = $rs;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(rs)
    }

    data class Add(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra + $rb;"
        override fun defs() = setOf(rd)
        override fun uses() = setOf(ra, rb)
    }

    data class Mul(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra * $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class Sub(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra - $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class Div(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra / $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class And(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra && $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class Or(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra || $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class CmpEQ(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra == $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class CmpNE(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra != $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class CmpGT(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra > $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class CmpGE(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra >= $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class CmpLT(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra < $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class CmpLE(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = $ra <= $rb;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }

    data class IntegerNeg(val rd: Var, val rs: Var) : IROperation() {
        override fun toString() = "$rd = -$rs;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(rs)
    }

    data class BooleanNeg(val rd: Var, val rs: Var) : IROperation() {
        override fun toString() = "$rd = !$rs;"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(rs)
    }

    data class BranchAlways(val target: Label) : IROperation() {
        override fun toString() = "branch-always(${target.name});"
        override fun defs(): Set<Var> = setOf()
        override fun uses(): Set<Var> = setOf()
    }

    data class BranchConditional(
        val cond: Var,
        val trueTarget: Label,
        val falseTarget: Label
    ) : IROperation() {
        override fun toString() = "branch-conditional($cond, ${trueTarget.name}, ${falseTarget.name});"
        override fun defs(): Set<Var> = setOf()
        override fun uses(): Set<Var> = setOf()
    }

    data class Label(val name: String) : IROperation() {
        override fun toString() = "$name:"
        override fun defs(): Set<Var> = setOf()
        override fun uses(): Set<Var> = setOf()
    }

    data class Phi(val rd: Var, val ra: Var, val rb: Var) : IROperation() {
        override fun toString() = "$rd = phi($ra, $rb);"
        override fun defs(): Set<Var> = setOf(rd)
        override fun uses(): Set<Var> = setOf(ra, rb)
    }
}
