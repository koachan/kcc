package kcc.parserlib

sealed class ParseResult<out T>
data class ParseSuccess<T>(val recognized: T, val remaining: ParseState) : ParseResult<T>()
data class ParseError<T>(val line: Int, val col: Int, val message: () -> String) : ParseResult<T>() {
    constructor(pe: ParseError<*>) : this(pe.line, pe.col, pe.message)
}

data class ParseState(val lines: List<String>, val line: Int, val col: Int) {
    fun currentChar(): Char {
        return lines[line][col]
    }

    fun currentSubstring(): String {
        return lines[line].substring(col)
    }

    fun currentSubstring(length: Int): String {
        return if (col + length <= lines[line].length) lines[line].substring(
            col,
            col + length
        ) else lines[line].substring(col)
    }

    fun moveForward(chars: Int): ParseState {
        var newLine = line
        var newCol = col
        var remaining = chars

        while (newLine < lines.size && newCol + remaining >= lines[newLine].length) {
            remaining -= (lines[newLine].length - newCol)
            newLine++
            newCol = 0
        }

        newCol += remaining

        return copy(line = newLine, col = newCol)
    }

    fun isEOF(): Boolean {
        return (line >= lines.size) || (line == lines.size - 1 && col >= lines[line].length)
    }
}

abstract class Parser<T> {
    fun parse(input: String): ParseResult<T> = parse(input.lines())
    fun parse(input: List<String>): ParseResult<T> = parse(ParseState(input, 0, 0))
    fun parse(input: ParseState): ParseResult<T> = parseImpl(input)
    protected abstract fun parseImpl(input: ParseState): ParseResult<T>
}

inline fun <T, U> Parser<T>.map(crossinline transform: (T) -> U): Parser<U> {
    val p = this
    return object : Parser<U>() {
        override fun parseImpl(input: ParseState): ParseResult<U> {
            return when (val r = p.parse(input)) {
                is ParseError -> ParseError(r)
                is ParseSuccess -> ParseSuccess(transform(r.recognized), r.remaining)
            }
        }
    }
}

inline fun <T> Parser<T>.filter(errorMessage: String, crossinline predicate: (T) -> Boolean): Parser<T> {
    val p = this
    return object : Parser<T>() {
        override fun parseImpl(input: ParseState): ParseResult<T> {
            return when (val r = p.parse(input)) {
                is ParseError -> ParseError(r)
                is ParseSuccess -> if (predicate(r.recognized))
                    ParseSuccess(r.recognized, r.remaining)
                else ParseError(input.line, input.col) { errorMessage }
            }
        }
    }
}
