package kcc.parserlib

class Skip<A>(private val p: Parser<A>) : Parser<Nothing?>() {
    override fun parseImpl(input: ParseState): ParseResult<Nothing?> {
        return when (val r = p.parse(input)) {
            is ParseError -> ParseError(r)
            is ParseSuccess -> ParseSuccess(null, r.remaining)
        }
    }
}

class AnyOf<A>(private vararg val ps: Parser<A>) : Parser<A>() {
    init { require(ps.isNotEmpty()) { "Parser list must not be empty" } }

    override fun parseImpl(input: ParseState): ParseResult<A> {
        var res: ParseResult<A>? = null
        for (p in ps) {
            res = p.parse(input)
            when(res) {
                is ParseError -> continue
                is ParseSuccess -> return res
            }
        }
        return res!!
    }
}

class Sequence<A>(private vararg val ps: Parser<A>) : Parser<List<A>>() {
    override fun parseImpl(input: ParseState): ParseResult<List<A>> {
        val resultList = mutableListOf<A>()
        var remaining = input
        for (p in ps) {
            when (val r = p.parse(remaining)) {
                is ParseError -> return ParseError(r)
                is ParseSuccess -> {
                    resultList.add(r.recognized)
                    remaining = r.remaining
                }
            }
        }

        return ParseSuccess(resultList, remaining)
    }
}

class ZeroOrOne<A>(private val p: Parser<A>) : Parser<A?>() {
    override fun parseImpl(input: ParseState): ParseResult<A?> {
        val r = p.parse(input)
        return if (r is ParseSuccess) ParseSuccess(r.recognized, r.remaining) else ParseSuccess(null, input)
    }
}

class ZeroOrMore<A>(private val p: Parser<A>) : Parser<List<A>>() {
    override fun parseImpl(input: ParseState): ParseResult<List<A>> {
        val resultList = ArrayList<A>()
        var remaining = input

        var r = p.parse(input)
        while (r is ParseSuccess) {
            resultList.add(r.recognized)
            remaining = r.remaining
            r = p.parse(remaining)
        }

        return ParseSuccess(resultList, remaining)
    }
}

class OneOrMore<A>(p: Parser<A>) : Parser<List<A>>() {
    private val q = p AndThen ZeroOrMore(p)

    override fun parseImpl(input: ParseState): ParseResult<List<A>> {
        return when (val r = q.parse(input)) {
            is ParseError -> ParseError(r)
            is ParseSuccess -> ParseSuccess(
                listOf(r.recognized.first) + r.recognized.second,
                r.remaining
            )
        }
    }
}

class Between<A, B, C>(p1: Parser<A>, p2: Parser<B>, p3: Parser<C>) : Parser<B>() {
    private val p = p1 KeepRight p2 KeepLeft p3

    override fun parseImpl(input: ParseState): ParseResult<B> = p.parse(input)
}

class Enclosing<A, B, C>(p1: Parser<A>, p2: Parser<B>, p3: Parser<C>) : Parser<Pair<A, C>>() {
    private val p = p1 KeepLeft p2 AndThen p3

    override fun parseImpl(input: ParseState): ParseResult<Pair<A, C>> = p.parse(input)
}

class Placeholder<A>(var parser: Parser<A>? = null) : Parser<A>() {
    override fun parseImpl(input: ParseState): ParseResult<A> = parser!!.parse(input)
}

// Several combinators are implemented as infix factory functions for ease of reading

infix fun <A, B> Parser<A>.AndThen(p2: Parser<B>): Parser<Pair<A, B>> {
    val p1 = this
    return object : Parser<Pair<A, B>>() {
        override fun parseImpl(input: ParseState): ParseResult<Pair<A, B>> {
            val r1 = p1.parse(input)
            val r2: ParseResult<B>

            val v1: A
            val v2: B
            val rem: ParseState

            when (r1) {
                is ParseError -> return ParseError(r1)
                is ParseSuccess<A> -> {
                    v1 = r1.recognized
                    r2 = p2.parse(r1.remaining)
                }
            }

            when (r2) {
                is ParseError -> return ParseError(r2)
                is ParseSuccess<B> -> {
                    v2 = r2.recognized
                    rem = r2.remaining
                }
            }

            return ParseSuccess(Pair(v1, v2), rem)
        }
    }
}

infix fun <A> Parser<A>.OrElse(p2: Parser<A>): Parser<A> {
    val p1 = this
    return object : Parser<A>() {
        override fun parseImpl(input: ParseState): ParseResult<A> {
            val r1 = p1.parse(input)
            return if (r1 is ParseSuccess) r1 else p2.parse(input)
        }
    }
}

infix fun <A, B> Parser<A>.KeepLeft(p2: Parser<B>): Parser<A> {
    val p1 = this
    return object : Parser<A>() {
        private val p = p1 AndThen p2

        override fun parseImpl(input: ParseState): ParseResult<A> {
            return when (val r = p.parse(input)) {
                is ParseError -> ParseError(r)
                is ParseSuccess -> ParseSuccess(r.recognized.first, r.remaining)
            }
        }
    }
}

infix fun <A, B> Parser<A>.KeepRight(p2: Parser<B>): Parser<B> {
    val p1 = this
    return object : Parser<B>() {
        private val p = p1 AndThen p2

        override fun parseImpl(input: ParseState): ParseResult<B> {
            return when (val r = p.parse(input)) {
                is ParseError -> ParseError(r)
                is ParseSuccess -> ParseSuccess(r.recognized.second, r.remaining)
            }
        }
    }
}
