package kcc.parserlib

class StringParser(private val string: String) : Parser<String>() {
    override fun parseImpl(input: ParseState): ParseResult<String> {
        if (input.isEOF()) {
            return ParseError(input.line, 0) { "End-of-file encountered" }
        }

        if (input.currentSubstring().startsWith(string)) {
            return ParseSuccess(string, input.moveForward(string.length))
        }

        return ParseError(input.line, input.col) {
            "Expecting '$string', got '${input.currentSubstring(string.length)}'\n" +
                    "${input.lines[input.line]}\n" +
                    " ".repeat(input.col) + "^"
        }
    }
}

class AnyOfString(private vararg val strings: String) : Parser<String>() {
    override fun parseImpl(input: ParseState): ParseResult<String> {
        if (input.isEOF()) {
            return ParseError(input.line, 0) { "End-of-file encountered" }
        }

        val res = strings.find { input.currentSubstring().startsWith(it) }
        if (res != null) {
            return ParseSuccess(res, input.moveForward(res.length))
        }

        return ParseError(input.line, input.col) {
            "Expecting one of ${strings.contentToString()} here\n" +
                    "${input.lines[input.line]}\n" +
                    " ".repeat(input.col) + "^"
        }
    }
}

class CharParser(private val char: Char) : Parser<Char>() {
    override fun parseImpl(input: ParseState): ParseResult<Char> {
        if (input.isEOF()) {
            return ParseError(input.line, 0) { "End-of-file encountered" }
        }

        if (input.currentChar() == char) {
            return ParseSuccess(char, input.moveForward(1))
        }

        return ParseError(input.line, input.col) {
            "Expecting '$char', got '${input.currentChar()}'\n" +
                    "${input.lines[input.line]}\n" +
                    " ".repeat(input.col) + "^"
        }
    }
}

class CharacterRangeParser(private val range: CharRange) : Parser<Char>() {
    override fun parseImpl(input: ParseState): ParseResult<Char> {
        if (input.isEOF()) {
            return ParseError(input.line, 0) { "End-of-file encountered" }
        }

        if (input.currentChar() in range) {
            return ParseSuccess(input.currentChar(), input.moveForward(1))
        }

        return ParseError(input.line, input.col) {
            "Expecting $range, got '${input.currentChar()}'\n" +
                    "${input.lines[input.line]}\n" +
                    " ".repeat(input.col) + "^"
        }
    }
}

class AnyOfChar(private vararg val chars: Char) : Parser<Char>() {
    override fun parseImpl(input: ParseState): ParseResult<Char> {
        if (input.isEOF()) {
            return ParseError(input.line, 0) { "End-of-file encountered" }
        }

        if (input.currentChar() in chars) {
            return ParseSuccess(input.currentChar(), input.moveForward(1))
        }

        return ParseError(input.line, input.col) {
            "Expecting one of ${chars.contentToString()} here\n" +
                    "${input.lines[input.line]}\n" +
                    " ".repeat(input.col) + "^"
        }
    }
}

class LowerCaseChar : Parser<Char>() {
    private val p = CharacterRangeParser('a'..'z')
    override fun parseImpl(input: ParseState): ParseResult<Char> = p.parse(input)
}

class UpperCaseChar : Parser<Char>() {
    private val p = CharacterRangeParser('A'..'Z')
    override fun parseImpl(input: ParseState): ParseResult<Char> = p.parse(input)
}

class NumericChar : Parser<Char>() {
    private val p = CharacterRangeParser('0'..'9')
    override fun parseImpl(input: ParseState): ParseResult<Char> = p.parse(input)
}

class AlphaChar : Parser<Char>() {
    private val p = LowerCaseChar() OrElse UpperCaseChar()
    override fun parseImpl(input: ParseState): ParseResult<Char> = p.parse(input)
}

class AlphaNumericChar : Parser<Char>() {
    private val p = NumericChar() OrElse AlphaChar()
    override fun parseImpl(input: ParseState): ParseResult<Char> = p.parse(input)
}

class NonNegativeInteger : Parser<String>() {
    override fun parseImpl(input: ParseState): ParseResult<String> =
        OneOrMore(NumericChar()).map { it.joinToString("") }.parse(input)
}

class ZeroOrMoreWhitespace : Parser<String>() {
    override fun parseImpl(input: ParseState): ParseResult<String> {
        if (input.isEOF()) {
            return ParseSuccess("", input)
        }

        // Minimal working impl, need to optimize
        var remaining = input
        var count = 0
        while (remaining.currentChar().isWhitespace()) {
            remaining = remaining.moveForward(1)
            count++
        }

        return ParseSuccess(" ".repeat(count), remaining)
    }
}
