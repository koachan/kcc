package kcc.lang

import kcc.ir.BasicBlock
import kcc.ir.CFGProgram
import kcc.ir.IROperation
import kcc.ir.Var

enum class BinaryOperators {
    ADD, MUL, SUB, DIV, // Arith
    LT, LE, GT, GE, EQ, NE, // Comparison
    AND, OR // Logic
}

enum class Type {
    INTEGER, BOOLEAN, NONE {
        override fun toString() = "NON-EXPRESSION"
    }
}

sealed class AST {
    abstract var type: Type? // Type of this expression node

    companion object {
        @JvmStatic
        // Static field for now, but need to be
        // changed into a per-tree field later.
        protected val executionState = ExecutionState()
    }

    fun evaluate(): CFGProgram {
        buildGraph()

        // I'm making some assumptions here:
        // - The AST will be evaluated in program order; so
        // - The basic blocks will also be generated in program order; hence
        // - The first basic block is our program entry point.

        return CFGProgram(executionState.cfg, executionState.cfg.nodes().first())
    }

    abstract fun buildGraph()
}

data class Program(val entrypoint: Scope) : AST() {
    override var type: Type? = Type.NONE

    override fun buildGraph() {
        entrypoint.buildGraph()
    }

    override fun toString() = "Program(\n${entrypoint.toString().prependIndent(" ")}\n)\n"
}

data class Scope(val statements: List<AST>) : AST() {
    override var type: Type? = Type.NONE

    override fun buildGraph() {
        executionState.currentNode.environment.addFrame()
        for (s in statements) s.buildGraph()
        executionState.currentNode.environment.deleteFrame()
    }

    override fun toString() = "Scope(\n${statements.joinToString("\n").prependIndent(" ")}\n)"
}

data class Declare(val id: Identifier, val statedType: Type, val value: AST) : AST() {
    override var type: Type? = Type.NONE // Declarations are typeless, it's not an expression.

    init {
        id.type = statedType // On declaration, initialize the identifier's type.
    }

    private fun typeError(left: Type, right: Type): Nothing =
        throw IllegalStateException("Assigning $right to a $left variable is not allowed")

    override fun buildGraph() {
        // Check the existence of the variable in the current frame first before putting anything.
        if (id.name in executionState.currentNode.environment.innermostFrame())
            throw IllegalStateException("Variable is already declared: ${id.name}")

        // After that, evaluate the `value` AST, then check the stated type
        // against the value's type.
        value.buildGraph()
        if (statedType != value.type!!) typeError(statedType, value.type!!) else id.type = statedType

        // This doesn't generate any instruction at all, only
        // notes the mapping between source variable name <-> SSA variable name
        // so we don't advance pc here.
        // Also, we explicitly assign it into the current frame.
        executionState.currentNode.environment.innermostFrame()[id.name] =
            Variable(value.type!!, Var(executionState.pc - 1))
    }
}

data class Assignment(val id: Identifier, val value: AST) : AST() {
    override var type: Type? = null

    private fun typeError(left: Type, right: Type): Nothing =
        throw IllegalStateException("Assigning $right to a $left variable is not allowed")

    override fun buildGraph() {
        // This is almost the same as the implmentation in Declare
        // with the exception that this will first check the existence of
        // the variable before assigning something to it.
        when (val v = executionState.currentNode.environment[id.name]) {
            null -> throw IllegalStateException("No such variable: ${id.name}")
            else -> id.type = v.type
        }

        // After that, evaluate the `value` AST, then check the stated type
        // against the value's type.
        value.buildGraph()
        if (id.type != value.type) typeError(id.type!!, value.type!!) else type = value.type

        // This doesn't generate any instruction at all, only
        // notes the mapping between source variable name <-> SSA variable name
        // so we don't advance pc here.
        executionState.currentNode.environment[id.name] = Variable(type!!, Var(executionState.pc - 1))
    }

}

data class Identifier(val name: String) : AST() {
    override var type: Type? = null

    override fun buildGraph() {
        when (val v = executionState.currentNode.environment[name]) {
            null -> throw IllegalStateException("No such variable: $name")
            else -> {
                type = v.type
                executionState.currentNode.block.ops.add(IROperation.Copy(Var(executionState.pc++), v.value))
            }
        }
    }
}

data class Expression(val op: BinaryOperators, val left: AST, val right: AST) : AST() {
    override var type: Type? = null

    private fun typeError(op: BinaryOperators, expectedType: Type): Nothing =
        throw IllegalStateException("$op can only bee used with $expectedType values")

    private fun typeError(left: AST, right: AST): Nothing =
        throw IllegalStateException("Mixing ${left.type} and ${right.type} is not allowed in this expression")

    override fun buildGraph() {
        left.buildGraph()
        val leftTerm = Var(executionState.pc - 1)

        right.buildGraph()
        val rightTerm = Var(executionState.pc - 1)

        val exprResult = Var(executionState.pc++)
        val insn = when (op) {
            BinaryOperators.ADD -> {
                if (left.type != Type.INTEGER) typeError(BinaryOperators.ADD, Type.INTEGER)
                if (left.type != right.type) typeError(left, right) else type = left.type
                IROperation.Add(exprResult, leftTerm, rightTerm)
            }
            BinaryOperators.MUL -> {
                if (left.type != Type.INTEGER) typeError(BinaryOperators.MUL, Type.INTEGER)
                if (left.type != right.type) typeError(left, right) else type = left.type
                IROperation.Mul(exprResult, leftTerm, rightTerm)
            }
            BinaryOperators.SUB -> {
                if (left.type != Type.INTEGER) typeError(BinaryOperators.SUB, Type.INTEGER)
                if (left.type != right.type) typeError(left, right) else type = left.type
                IROperation.Sub(exprResult, leftTerm, rightTerm)
            }
            BinaryOperators.DIV -> {
                if (left.type != Type.INTEGER) typeError(BinaryOperators.DIV, Type.INTEGER)
                if (left.type != right.type) typeError(left, right) else type = left.type
                IROperation.Div(exprResult, leftTerm, rightTerm)
            }

            BinaryOperators.AND -> {
                if (left.type != Type.BOOLEAN) typeError(BinaryOperators.AND, Type.BOOLEAN)
                if (left.type != right.type) typeError(left, right) else type = left.type
                IROperation.And(exprResult, leftTerm, rightTerm)
            }
            BinaryOperators.OR -> {
                if (left.type != Type.BOOLEAN) typeError(BinaryOperators.OR, Type.BOOLEAN)
                if (left.type != right.type) typeError(left, right) else type = left.type
                IROperation.Or(exprResult, leftTerm, rightTerm)
            }

            BinaryOperators.EQ -> {
                if (left.type != right.type) typeError(left, right) else type = Type.BOOLEAN
                IROperation.CmpEQ(exprResult, leftTerm, rightTerm)
            }
            BinaryOperators.NE -> {
                if (left.type != right.type) typeError(left, right) else type = Type.BOOLEAN
                IROperation.CmpNE(exprResult, leftTerm, rightTerm)
            }

            BinaryOperators.LT -> {
                if (left.type != Type.INTEGER) typeError(BinaryOperators.LT, Type.INTEGER)
                if (left.type != right.type) typeError(left, right) else type = Type.BOOLEAN
                IROperation.CmpLT(exprResult, leftTerm, rightTerm)
            }
            BinaryOperators.LE -> {
                if (left.type != Type.INTEGER) typeError(BinaryOperators.LE, Type.INTEGER)
                if (left.type != right.type) typeError(left, right) else type = Type.BOOLEAN
                IROperation.CmpLE(exprResult, leftTerm, rightTerm)
            }
            BinaryOperators.GT -> {
                if (left.type != Type.INTEGER) typeError(BinaryOperators.GT, Type.INTEGER)
                if (left.type != right.type) typeError(left, right) else type = Type.BOOLEAN
                IROperation.CmpGT(exprResult, leftTerm, rightTerm)
            }
            BinaryOperators.GE -> {
                if (left.type != Type.INTEGER) typeError(BinaryOperators.GE, Type.INTEGER)
                if (left.type != right.type) typeError(left, right) else type = Type.BOOLEAN
                IROperation.CmpGE(exprResult, leftTerm, rightTerm)
            }
        }

        executionState.currentNode.block.ops.add(insn)
    }
}

data class IfClause(val cond: AST, val trueBranch: Scope, val falseBranch: Scope) : AST() {
    override var type: Type? = Type.NONE // If clauses isn't an expression in this language

    private fun typeError(): Nothing =
        throw IllegalStateException("The condition part of an if clause must be a ${Type.BOOLEAN} expression")

    override fun buildGraph() {
        // First, evaluate the conditional and save the environment table of the state before branching.
        cond.buildGraph(); if (cond.type != Type.BOOLEAN) typeError()
        val preIfBlock = executionState.currentNode.block
        val preIfTable = executionState.currentNode.environment

        // Second, allocate two basic blocks and tables to contain the false and true branches.
        val trueLabel = IROperation.Label(".Lbb_if_true_pc${executionState.pc}")
        val falseLabel = IROperation.Label(".Lbb_if_false_pc${executionState.pc}")

        var trueBlock = BasicBlock(trueLabel)
        var falseBlock = BasicBlock(falseLabel)

        var trueTable = EnvironmentTable(preIfTable)
        var falseTable = EnvironmentTable(preIfTable)

        // The label for the control flow merge needs to be generated right now so that
        // the branches know where to jump to when they are done executing.
        val postIfLabel = IROperation.Label(".Lbb_if_merge_pc${executionState.pc}")

        // Third, using the condition, emit a branch operation on current basic block.
        val condition = Var(executionState.pc - 1)
        executionState.currentNode.block.ops.add(IROperation.BranchConditional(condition, trueLabel, falseLabel))

        // Fourth, generate code for the true and false branches.
        executionState.currentNode = ExecutionState.Node(trueBlock, trueTable)
        trueBranch.buildGraph()
        trueBlock = executionState.currentNode.block
        trueTable = executionState.currentNode.environment

        executionState.currentNode = ExecutionState.Node(falseBlock, falseTable)
        falseBranch.buildGraph()
        falseBlock = executionState.currentNode.block
        falseTable = executionState.currentNode.environment

        // Fifth, set up the post-if-clause block, and generate phi operations
        // and update the environment table as necessary.
        val postIfBlock = BasicBlock(postIfLabel)
        executionState.currentNode = ExecutionState.Node(postIfBlock, EnvironmentTable(preIfTable))

        val tableDifferences = trueTable diff falseTable
        for (d in tableDifferences) {
            if (d.v1.type != d.v2.type) throw IllegalStateException("Found different types when generating phi: $d")

            val phiResult = Var(executionState.pc++)
            executionState.currentNode.block.ops.add(IROperation.Phi(phiResult, d.v1.value, d.v2.value))
            executionState.currentNode.environment[d.name] = Variable(d.v1.type, phiResult)
        }

        // Sixth, generate the merging branches, and set up the CFG flows.
        trueBlock.ops.add(IROperation.BranchAlways(postIfLabel))
        falseBlock.ops.add(IROperation.BranchAlways(postIfLabel))

        executionState.cfg.addFlow(preIfBlock, trueBlock)
        executionState.cfg.addFlow(preIfBlock, falseBlock)
        executionState.cfg.addFlow(trueBlock, postIfBlock)
        executionState.cfg.addFlow(falseBlock, postIfBlock)
    }

    override fun toString() = "IfClause(cond=$cond,\n" +
            "         trueBranch=\n${trueBranch.toString().prependIndent("         ")},\n" +
            "         falseBranch=\n${falseBranch.toString().prependIndent("         ")})"
}

data class IntegerLit(val value: Long) : AST() {
    override var type: Type? = Type.INTEGER

    override fun buildGraph() {
        executionState.currentNode.block.ops.add(IROperation.ImmI64(Var(executionState.pc++), value))
    }
}

data class BooleanLit(val value: Boolean) : AST() {
    override var type: Type? = Type.BOOLEAN

    override fun buildGraph() {
        executionState.currentNode.block.ops.add(IROperation.ImmBoolean(Var(executionState.pc++), value))
    }
}

data class IntegerNegate(val expr: AST) : AST() {
    override var type: Type? = Type.INTEGER

    private fun typeError(expr: AST): Nothing =
        throw IllegalStateException("Integer negation is not supported for ${expr.type}")

    override fun buildGraph() {
        expr.buildGraph()
        val exprResult = executionState.pc - 1

        if (expr.type != Type.INTEGER) typeError(expr)
        executionState.currentNode.block.ops.add(IROperation.IntegerNeg(Var(executionState.pc++), Var(exprResult)))
    }
}

data class BooleanNegate(val expr: AST) : AST() {
    override var type: Type? = Type.BOOLEAN

    private fun typeError(expr: AST): Nothing =
        throw IllegalStateException("Boolean negation is not supported for ${expr.type}")

    override fun buildGraph() {
        expr.buildGraph()
        val exprResult = executionState.pc - 1

        if (expr.type != Type.BOOLEAN) typeError(expr)
        executionState.currentNode.block.ops.add(IROperation.BooleanNeg(Var(executionState.pc++), Var(exprResult)))
    }
}
