package kcc.lang

import kcc.parserlib.*

/* The grammar for the language, adapted from the original description at
   https://github.com/zakirullin/tiny-compiler#the-language-description-in-ebnf
   with (major) modifications.

   Number:     [0-9]+
   Boolean:    "true" | "false"
   Type:       "integer" | "boolean"
   Identifier: [a-zA-Z][0-9a-zA-Z]*

   Assignment: Identifier '=' Expr

   Term:      Identifier | Number | Boolean | '(' Expr ')'
   Negation:  ("!" | "-")? Term
   BinaryOp5: Negation (("*" | "/") Negation)*
   BinaryOp4: BinaryOp5 (("+" | "-") BinaryOp5)*
   BinaryOp3: BinaryOp4 (("!=" | "==" | "<=" | "<" | ">=" | ">" ) BinaryOp4)*
   BinaryOp2: BinaryOp3 ("&&" BinaryOp3)*
   BinaryOp1: BinaryOp2 ("||" BinaryOp2)*

   Expr:      Assignment | BinaryOp1
   Decl:      "declare" Identifier Type '=' Expr
   Statement: ((Expr | Decl) ';') | IfClause
   Scope:     '{' Lines* '}'
   IfClause:  "if" Expr "then" Scope "else" Scope
   Lines:     (Statement | Scope)
   Program:   Lines+ #EOF
 */

// This is like OneOrMore but tries to parse all the way to EOF, and
// immediately bails out upon encountering an error instead of backtracking.
class Iterate<A>(private val p: Parser<A>) : Parser<List<A>>() {
    override fun parseImpl(input: ParseState): ParseResult<List<A>> {
        val resultList = ArrayList<A>()
        var remaining = input

        while (!remaining.isEOF()) {
            when (val res = p.parse(remaining)) {
                is ParseSuccess -> {
                    resultList.add(res.recognized)
                    remaining = res.remaining
                }
                is ParseError -> return ParseError(res)
            }
        }

        return ParseSuccess(resultList, remaining)
    }
}

class TermParser<A>(
    identParser: Parser<A>,
    innerExprParser: Parser<A>,
    vararg litParser: Parser<A>
) : Parser<A>() {
    private val p = AnyOf(
        identParser,
        *litParser,
        Between(
            CharParser('(') AndThen ZeroOrMoreWhitespace(),
            innerExprParser KeepLeft ZeroOrMoreWhitespace(),
            CharParser(')')
        )
    )

    override fun parseImpl(input: ParseState): ParseResult<A> {
        return p.parse(input)
    }

}

fun parseLanguage(input: List<String>): Result<AST> {
    val reservedWords = hashSetOf("true", "false", "declare", "integer", "boolean", "if", "then", "else")
    val whitespace = ZeroOrMoreWhitespace()

    val number: Parser<AST> = NonNegativeInteger().map { IntegerLit(it.toLong()) }
    val boolean: Parser<AST> = AnyOfString("true", "false")
        .map { BooleanLit(it.toBooleanStrict()) }
    val type = AnyOfString("integer", "boolean").map {
        when (it) {
            "integer" -> Type.INTEGER
            "boolean" -> Type.BOOLEAN
            else -> throw IllegalStateException("Type must be either 'integer' or 'boolean'")
        }
    }
    val identifier: Parser<AST> = (AlphaChar() AndThen ZeroOrMore(AlphaNumericChar()))
        .map { Identifier(it.first.toString() + it.second.joinToString("")) }
        .filter("Encountered reserved word") { it.name !in reservedWords }.map { it }

    val exprPlaceholder = Placeholder<AST>()
    val assignmentOp = CharParser('=') AndThen whitespace
    val assignment: Parser<AST> =
        ((identifier KeepLeft whitespace) AndThen (assignmentOp KeepRight exprPlaceholder)).map {
            Assignment(
                it.first as Identifier,
                it.second
            )
        }

    val term = TermParser(identifier, exprPlaceholder, number, boolean) KeepLeft whitespace
    val negateIntegerOp = CharParser('-') AndThen whitespace
    val negateIntegerExpr = (ZeroOrMore(negateIntegerOp) AndThen term).map {
        if (it.first.size % 2 != 0) IntegerNegate(it.second) else it.second
    }
    val negateBooleanOp = CharParser('!') AndThen whitespace
    val negateBooleanExpr = (ZeroOrMore(negateBooleanOp) AndThen negateIntegerExpr).map {
        if (it.first.size % 2 != 0) BooleanNegate(it.second) else it.second
    }

    val binOp5 = AnyOfChar('*', '/') KeepLeft whitespace
    val binExpr5 = (negateBooleanExpr AndThen ZeroOrMore(binOp5 AndThen negateBooleanExpr)).map {
        when (it.second.size) {
            0 -> it.first
            else -> {
                var ex: AST = it.first
                for (op in it.second) {
                    ex = when (op.first) {
                        '*' -> Expression(BinaryOperators.MUL, ex, op.second)
                        '/' -> Expression(BinaryOperators.DIV, ex, op.second)
                        else -> throw IllegalStateException("Expecting '*' or '/'")
                    }
                }
                ex
            }
        }
    }
    val binOp4 = AnyOfChar('+', '-') KeepLeft whitespace
    val binExpr4 = (binExpr5 AndThen ZeroOrMore(binOp4 AndThen binExpr5)).map {
        when (it.second.size) {
            0 -> it.first
            else -> {
                var ex: AST = it.first
                for (op in it.second) {
                    ex = when (op.first) {
                        '+' -> Expression(BinaryOperators.ADD, ex, op.second)
                        '-' -> Expression(BinaryOperators.SUB, ex, op.second)
                        else -> throw IllegalStateException("Expecting '+' or '-'")
                    }
                }
                ex
            }
        }
    }
    val binOp3 = AnyOfString("<=", "<", ">=", ">", "==", "!=") KeepLeft whitespace
    val binExpr3 = (binExpr4 AndThen ZeroOrMore(binOp3 AndThen binExpr4)).map {
        when (it.second.size) {
            0 -> it.first
            else -> {
                var ex: AST = it.first
                for (op in it.second) {
                    ex = when (op.first) {
                        "<=" -> Expression(BinaryOperators.LE, ex, op.second)
                        "<" -> Expression(BinaryOperators.LT, ex, op.second)
                        ">=" -> Expression(BinaryOperators.GE, ex, op.second)
                        ">" -> Expression(BinaryOperators.GT, ex, op.second)
                        "!=" -> Expression(BinaryOperators.NE, ex, op.second)
                        "==" -> Expression(BinaryOperators.EQ, ex, op.second)
                        else -> throw IllegalStateException("Expecting '<=', '<', '>=', '!=' or '=='")
                    }
                }
                ex
            }
        }
    }
    val binOp2 = StringParser("&&") AndThen whitespace
    val binExpr2 = (binExpr3 AndThen ZeroOrMore(binOp2 KeepRight binExpr3)).map {
        when (it.second.size) {
            0 -> it.first
            else -> {
                var ex: AST = it.first
                for (op in it.second) {
                    ex = Expression(BinaryOperators.AND, ex, op)
                }
                ex
            }
        }
    }
    val binOp1 = StringParser("||") AndThen whitespace
    val binExpr1 = (binExpr2 AndThen ZeroOrMore(binOp1 KeepRight binExpr2)).map {
        when (it.second.size) {
            0 -> it.first
            else -> {
                var ex: AST = it.first
                for (op in it.second) {
                    ex = Expression(BinaryOperators.OR, ex, op)
                }
                ex
            }
        }
    }

    val declOp = StringParser("declare") AndThen whitespace
    val declId = identifier KeepLeft whitespace
    val declTy = type KeepLeft whitespace
    val declEx = assignmentOp KeepRight exprPlaceholder
    val decl: Parser<AST> = declOp KeepRight (declId AndThen declTy AndThen declEx).map {
        Declare(it.first.first as Identifier, it.first.second, it.second)
    }

    val expr = assignment OrElse binExpr1
    exprPlaceholder.parser = expr

    val ifClausePlaceholder = Placeholder<AST>()
    val statement = whitespace KeepRight (((expr OrElse decl) KeepLeft whitespace KeepLeft CharParser(';'))
            OrElse ifClausePlaceholder)

    val linesPlaceholder = Placeholder<AST>()
    val scope: Parser<AST> = whitespace KeepRight
            Between(
                CharParser('{') KeepLeft whitespace,
                ZeroOrMore(linesPlaceholder) KeepLeft whitespace,
                CharParser('}')
            ).map {
                Scope(it)
            }

    val ifClause: Parser<AST> = (Between(
        StringParser("if") AndThen whitespace,
        expr,
        StringParser("then") AndThen whitespace
    ) AndThen Enclosing(
        scope KeepLeft whitespace, StringParser("else") AndThen whitespace, scope
    )).map {
        IfClause(it.first, it.second.first as Scope, it.second.second as Scope)
    }
    ifClausePlaceholder.parser = ifClause

    val lines = statement OrElse scope
    linesPlaceholder.parser = lines

    val program: Parser<AST> = Iterate(lines).map { Program(Scope(it)) }

    return when (val r = program.parse(input)) {
        is ParseSuccess -> Result.success(r.recognized)
        is ParseError -> Result.failure(IllegalStateException("Error parsing source: ${r.line + 1}:${r.col + 1}: ${r.message()}"))
    }
}
