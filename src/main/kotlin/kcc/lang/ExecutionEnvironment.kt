package kcc.lang

import kcc.ir.BasicBlock
import kcc.ir.ControlFlowGraph
import kcc.ir.IROperation
import kcc.ir.Var

data class Variable(val type: Type, val value: Var)

data class EnvironmentTable(private val env: ArrayList<HashMap<String, Variable>> = ArrayList()) {
    constructor(evt: EnvironmentTable) : this(ArrayList(evt.env.size)) {
        for (frame in evt.env) {
            env += HashMap(frame)
        }
    }

    private fun findTable(k: String): HashMap<String, Variable>? {
        for (i in (env.size - 1) downTo 0) {
            if (k in env[i]) return env[i]
        }

        return null
    }

    fun depth() = env.size

    fun frames(): List<Map<String, Variable>> = env

    fun addFrame() { env += HashMap() }

    fun deleteFrame() { env.removeLast() }

    fun innermostFrame() = env.last()

    operator fun contains(k: String) = get(k) != null

    operator fun get(k: String): Variable? {
        return when (val tb = findTable(k)) {
            null -> null
            else -> tb[k]
        }
    }

    operator fun set(k: String, v: Variable) {
        when (val tb = findTable(k)) {
            null -> env.last()[k] = v
            else -> tb[k] = v
        }
    }
}

data class ExecutionState(
    var pc: Int = 0,
    val cfg: ControlFlowGraph = ControlFlowGraph(),
    var currentNode: Node = Node(BasicBlock(IROperation.Label(".Lbb_init")), EnvironmentTable())
) {
    data class Node(val block: BasicBlock, val environment: EnvironmentTable)

    init {
        cfg.addBlock(currentNode.block)
    }
}

data class Difference(val name: String, val v1: Variable, val v2: Variable)

infix fun EnvironmentTable.diff(other: EnvironmentTable): List<Difference> {
    // I'm making some assumptions here:
    // - `this` and `other` has the same depth; and
    // - Each frame in `this` and `other` contains the same set of keys.
    // In other words, `this` and `other` is the same in every aspect,
    // except in the definition of some of the variables.

    if (this.depth() != other.depth())
        throw IllegalStateException("Running `diff` on tables of different depth isn't allowed")

    val thisFrames = this.frames()
    val otherFrames = other.frames()

    val differences = ArrayList<Difference>()

    for ((i, _) in thisFrames.withIndex()) {
        val t = thisFrames[i]
        val o = otherFrames[i]

        if (t.keys != o.keys)
            throw IllegalStateException("Running `diff` on tables that defines different set of variables isn't allowed")

        for ((k, v) in t) {
            if (v != o[k]!!) differences.add(Difference(k, v, o[k]!!))
        }
    }

    return differences
}
