package kcc.backends.powerpc64

internal sealed interface StorageKind {
    data class Var(val num: Int) : StorageKind {
        override fun toString() = "var($num)"
    }

    enum class Reg : StorageKind {
        r0, // For function linkage (temporary LR storage, etc.)
        r1, // Stack pointer
        r2, // TOC pointer
        r3, r4, // Parameters & return values (can also be used as scratch area)
        r5, r6, r7, r8, r9, r10, // Additional parameters (can also be used as scratch area)
        r11, // Environment pointer
        r12, // Function GEP
        r13, // TLS pointer
        r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, // Nonvolatile scratch area
        r31  // Frame pointer
    }
}

internal enum class Width {
    Byte, // 8-bit
    Halfword, // 16-bit
    Word, // 32-bit
    Doubleword // 64-bit
}

internal enum class ConditionReg {
    cr0, cr1, cr2, cr3, cr4, cr5, cr6, cr7,
}

internal sealed class Instruction<T : StorageKind> {
    abstract fun defs(): Set<T>
    abstract fun uses(): Set<T>

    data class LoadImmediate<T : StorageKind>(val rd: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "li %$rd, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf()
    }

    data class LoadImmediateShifted<T : StorageKind>(val rd: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "lis %$rd, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf()
    }

    data class Load<T : StorageKind>(val rd: T, val ps: T, val d: Short, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Byte -> "lbz %$rd, $d(%$ps)"
                Width.Halfword -> "lhz %$rd, $d(%$ps)"
                Width.Word -> "lwz %$rd, $d(%$ps)"
                Width.Doubleword -> "ld %$rd, $d(%$ps)"
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ps)
    }

    data class LoadAndUpdate<T : StorageKind>(val rd: T, val ps: T, val d: Short, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Byte -> "lbzu %$rd, $d(%$ps)"
                Width.Halfword -> "lhzu %$rd, $d(%$ps)"
                Width.Word -> "lwzu %$rd, $d(%$ps)"
                Width.Doubleword -> "ldu %$rd, $d(%$ps)"
            }
        }

        override fun defs(): Set<T> = setOf(rd, ps)
        override fun uses(): Set<T> = setOf(rd, ps)
    }

    data class Store<T : StorageKind>(val rs: T, val pd: T, val d: Short, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Byte -> "stb %$rs, $d(%$pd)"
                Width.Halfword -> "sth %$rs, $d(%$pd)"
                Width.Word -> "stw %$rs, $d(%$pd)"
                Width.Doubleword -> "std %$rs, $d(%$pd)"
            }
        }

        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf(rs, pd)
    }

    data class StoreAndUpdate<T : StorageKind>(val rs: T, val pd: T, val d: Short, val width: Width) :
        Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Byte -> "stbu %$rs, $d(%$pd)"
                Width.Halfword -> "sthu %$rs, $d(%$pd)"
                Width.Word -> "stwu %$rs, $d(%$pd)"
                Width.Doubleword -> "stdu %$rs, $d(%$pd)"
            }
        }

        override fun defs(): Set<T> = setOf(pd)
        override fun uses(): Set<T> = setOf(rs, pd)
    }

    data class Move<T : StorageKind>(val rd: T, val rs: T) : Instruction<T>() {
        override fun toString(): String = "mr %$rd, %$rs"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(rs)
    }

    data class MoveToLR<T : StorageKind>(val rs: T) : Instruction<T>() {
        override fun toString(): String = "mtlr %$rs"
        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf(rs)
    }

    data class MoveFromLR<T : StorageKind>(val rd: T) : Instruction<T>() {
        override fun toString(): String = "mflr %$rd"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf()
    }

    data class Add<T : StorageKind>(val rd: T, val ra: T, val rb: T) : Instruction<T>() {
        override fun toString(): String = "add %$rd, %$ra, %$rb"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class AddImmediate<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "addi %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class AddImmediateShifted<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "addis %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class Mul<T : StorageKind>(val rd: T, val ra: T, val rb: T, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "mullw %$rd, %$ra, %$rb"
                Width.Doubleword -> "mulld %$rd, %$ra, %$rb"
                else -> throw IllegalStateException("mull* can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class MulH<T : StorageKind>(val rd: T, val ra: T, val rb: T, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "mulhw %$rd, %$ra, %$rb"
                Width.Doubleword -> "mulhd %$rd, %$ra, %$rb"
                else -> throw IllegalStateException("mulh* can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class Sub<T : StorageKind>(val rd: T, val ra: T, val rb: T) : Instruction<T>() {
        override fun toString(): String = "sub %$rd, %$ra, %$rb"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class SubImmediate<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "subi %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class SubImmediateShifted<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "subis %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class Div<T : StorageKind>(val rd: T, val ra: T, val rb: T, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "divw %$rd, %$ra, %$rb"
                Width.Doubleword -> "divd %$rd, %$ra, %$rb"
                else -> throw IllegalStateException("div* can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class Neg<T : StorageKind>(val rd: T, val rs: T) : Instruction<T>() {
        override fun toString(): String = "neg %$rd, %$rs"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(rs)
    }

    data class And<T : StorageKind>(val rd: T, val ra: T, val rb: T) : Instruction<T>() {
        override fun toString(): String = "and %$rd, %$ra, %$rb"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class AndImmediate<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "andi %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class AndImmediateShifted<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "andis %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class Or<T : StorageKind>(val rd: T, val ra: T, val rb: T) : Instruction<T>() {
        override fun toString(): String = "or %$rd, %$ra, %$rb"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class OrImmediate<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "ori %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class OrImmediateShifted<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "oris %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class Xor<T : StorageKind>(val rd: T, val ra: T, val rb: T) : Instruction<T>() {
        override fun toString(): String = "xor %$rd, %$ra, %$rb"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class XorImmediate<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "xori %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class XorImmediateShifted<T : StorageKind>(val rd: T, val ra: T, val imm: Short) : Instruction<T>() {
        override fun toString(): String = "xoris %$rd, %$ra, $imm"
        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class CountLeadingZero<T : StorageKind>(val rd: T, val rs: T, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "cntlzw %$rd, %$rs"
                Width.Doubleword -> "cntlzd %$rd, %$rs"
                else -> throw IllegalStateException("cntlz* can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(rs)
    }

    data class ShiftLeft<T : StorageKind>(val rd: T, val ra: T, val rb: T, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "slw %$rd, %$ra, %$rb"
                Width.Doubleword -> "sld %$rd, %$ra, %$rb"
                else -> throw IllegalStateException("sl* can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class ShiftLeftImmediate<T : StorageKind>(val rd: T, val ra: T, val imm: Short, val width: Width) :
        Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "slwi %$rd, %$ra, $imm"
                Width.Doubleword -> "sldi %$rd, %$ra, $imm"
                else -> throw IllegalStateException("sl*i can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class ShiftRight<T : StorageKind>(val rd: T, val ra: T, val rb: T, val width: Width) : Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "srw %$rd, %$ra, %$rb"
                Width.Doubleword -> "srd %$rd, %$ra, %$rb"
                else -> throw IllegalStateException("sr* can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class ShiftRightImmediate<T : StorageKind>(val rd: T, val ra: T, val imm: Short, val width: Width) :
        Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "srwi %$rd, %$ra, $imm"
                Width.Doubleword -> "srdi %$rd, %$ra, $imm"
                else -> throw IllegalStateException("sr*i can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class ShiftRightAlgebraic<T : StorageKind>(val rd: T, val ra: T, val rb: T, val width: Width) :
        Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "sraw %$rd, %$ra, %$rb"
                Width.Doubleword -> "srad %$rd, %$ra, %$rb"
                else -> throw IllegalStateException("sra* can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra, rb)
    }

    data class ShiftRightAlgebraicImmediate<T : StorageKind>(val rd: T, val ra: T, val imm: Short, val width: Width) :
        Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "srawi %$rd, %$ra, $imm"
                Width.Doubleword -> "sradi %$rd, %$ra, $imm"
                else -> throw IllegalStateException("sra*i can only be usedwith word or doubleword integers")
            }
        }

        override fun defs(): Set<T> = setOf(rd)
        override fun uses(): Set<T> = setOf(ra)
    }

    data class Compare<T : StorageKind>(val cr: ConditionReg = ConditionReg.cr0, val ra: T, val rb: T, val width: Width) :
        Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "cmpw %$cr, %$ra, %$rb"
                Width.Doubleword -> "cmpd %$cr, %$ra, %$rb"
                else -> throw IllegalStateException("cmp* can only be usedwith word or doubleword integers")
            }
        }
        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf(rb)
    }

    data class CompareImmediate<T : StorageKind>(val cr: ConditionReg = ConditionReg.cr0, val ra: T, val imm: Short, val width: Width) :
        Instruction<T>() {
        override fun toString(): String {
            return when (width) {
                Width.Word -> "cmpwi %$cr, %$ra, $imm"
                Width.Doubleword -> "cmpdi %$cr, %$ra, $imm"
                else -> throw IllegalStateException("cmp* can only be usedwith word or doubleword integers")
            }
        }
        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf(ra)
    }

    data class Label<T : StorageKind>(val name: String) : Instruction<T>() {
        override fun toString(): String = "$name:"
        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf()
    }

    data class BranchAlways<T : StorageKind>(val target: Label<T>) : Instruction<T>() {
        override fun toString(): String = "b ${target.name}"
        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf()
    }

    data class BranchIfEqual<T : StorageKind>(val cr: ConditionReg = ConditionReg.cr0, val target: Label<T>) :
        Instruction<T>() {
        override fun toString(): String = "beq %$cr, ${target.name}"
        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf()
    }

    data class BranchIfNotEqual<T : StorageKind>(val cr: ConditionReg = ConditionReg.cr0, val target: Label<T>) :
        Instruction<T>() {
        override fun toString(): String = "bne %$cr, ${target.name}"
        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf()
    }

    class BranchToLR<T : StorageKind> : Instruction<T>() {
        override fun toString(): String = "blr"
        override fun defs(): Set<T> = setOf()
        override fun uses(): Set<T> = setOf()
    }
}
