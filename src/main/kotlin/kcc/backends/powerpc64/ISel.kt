package kcc.backends.powerpc64
import kcc.ir.IROperation
import kcc.ir.LinearProgram
import kcc.ir.Var

internal data class AbstractMachineState(val environment: HashMap<Var, StorageKind.Var> = LinkedHashMap(), var currentVar: Int = 0) {
    fun allocateNewVar(irVar: Var): StorageKind.Var {
        val v = environment[irVar]
        if (v != null) return v

        val rd = StorageKind.Var(currentVar++)
        environment[irVar] = rd
        return rd
    }
}

internal fun ppc64InstructionSelect(p: LinearProgram<IROperation>): LinearProgram<Instruction<StorageKind.Var>> {
    val ams = AbstractMachineState()
    val ops = ArrayList<Instruction<StorageKind.Var>>()

    for (op in p.ops) {
            when(op) {
                is IROperation.ImmBoolean -> ops.add(Instruction.LoadImmediate(ams.allocateNewVar(op.rd), if (op.imm) 1 else 0))
                is IROperation.ImmI64 -> {
                    val rd = ams.allocateNewVar(op.rd)
                    ops.addAll(when (op.imm) {
                        in Short.MIN_VALUE..Short.MAX_VALUE -> listOf(Instruction.LoadImmediate(rd, op.imm.toShort()))
                        in Int.MIN_VALUE..Int.MAX_VALUE -> listOf(
                            Instruction.LoadImmediateShifted(rd, (op.imm shr 16).toShort()),
                            Instruction.OrImmediate(rd, rd, op.imm.toShort()),
                        )
                        else -> listOf(
                            Instruction.LoadImmediateShifted(rd, (op.imm shr 48).toShort()),
                            Instruction.OrImmediate(rd, rd, (op.imm shr 32).toShort()),
                            Instruction.ShiftLeftImmediate(rd, rd, 32, Width.Doubleword),
                            Instruction.OrImmediateShifted(rd, rd, (op.imm shr 16).toShort()),
                            Instruction.OrImmediate(rd, rd, op.imm.toShort())
                        )
                    })
                }

                is IROperation.Copy -> ops.add(Instruction.Move(ams.allocateNewVar(op.rd), ams.environment[op.rs]!!))

                is IROperation.Add -> ops.add(Instruction.Add(ams.allocateNewVar(op.rd), ams.environment[op.ra]!!, ams.environment[op.rb]!!))
                is IROperation.Mul -> ops.add(Instruction.Mul(ams.allocateNewVar(op.rd), ams.environment[op.ra]!!, ams.environment[op.rb]!!, Width.Doubleword))
                is IROperation.Sub -> ops.add(Instruction.Sub(ams.allocateNewVar(op.rd), ams.environment[op.ra]!!, ams.environment[op.rb]!!))
                is IROperation.Div -> ops.add(Instruction.Div(ams.allocateNewVar(op.rd), ams.environment[op.ra]!!, ams.environment[op.rb]!!, Width.Doubleword))

                is IROperation.And -> ops.add(Instruction.And(ams.allocateNewVar(op.rd), ams.environment[op.ra]!!, ams.environment[op.rb]!!))
                is IROperation.Or -> ops.add(Instruction.Or(ams.allocateNewVar(op.rd), ams.environment[op.ra]!!, ams.environment[op.rb]!!))

                is IROperation.CmpEQ -> {
                    val rd = ams.allocateNewVar(op.rd)
                    ops.addAll(listOf(
                        Instruction.Xor(rd, ams.environment[op.ra]!!, ams.environment[op.rb]!!),
                        Instruction.CountLeadingZero(rd, rd, Width.Doubleword),
                        Instruction.ShiftRightImmediate(rd, rd, 6, Width.Word)
                    ))
                }
                is IROperation.CmpNE -> {
                    val rd = ams.allocateNewVar(op.rd)
                    ops.addAll(listOf(
                        Instruction.Xor(rd, ams.environment[op.ra]!!, ams.environment[op.rb]!!),
                        Instruction.CountLeadingZero(rd, rd, Width.Doubleword),
                        Instruction.ShiftRightImmediate(rd, rd, 6, Width.Word),
                        Instruction.XorImmediate(rd, rd, 1)
                    ))
                }
                is IROperation.CmpGE -> {
                    val rd = ams.allocateNewVar(op.rd)
                    ops.addAll(listOf(
                        Instruction.Sub(rd, ams.environment[op.ra]!!, ams.environment[op.rb]!!),
                        Instruction.ShiftRightImmediate(rd, rd, 63, Width.Doubleword),
                        Instruction.XorImmediate(rd, rd, 1)
                    ))
                }
                is IROperation.CmpGT -> {
                    val rd = ams.allocateNewVar(op.rd)
                    ops.addAll(listOf(
                        Instruction.Sub(rd, ams.environment[op.rb]!!, ams.environment[op.ra]!!),
                        Instruction.ShiftRightImmediate(rd, rd, 63, Width.Doubleword),
                    ))
                }
                is IROperation.CmpLE -> {
                    val rd = ams.allocateNewVar(op.rd)
                    ops.addAll(listOf(
                        Instruction.Sub(rd, ams.environment[op.rb]!!, ams.environment[op.ra]!!),
                        Instruction.ShiftRightImmediate(rd, rd, 63, Width.Doubleword),
                        Instruction.XorImmediate(rd, rd, 1)
                    ))
                }
                is IROperation.CmpLT -> {
                    val rd = ams.allocateNewVar(op.rd)
                    ops.addAll(listOf(
                        Instruction.Sub(rd, ams.environment[op.ra]!!, ams.environment[op.rb]!!),
                        Instruction.ShiftRightImmediate(rd, rd, 63, Width.Doubleword),
                    ))
                }

                is IROperation.Label -> ops.add(Instruction.Label(op.name))
                is IROperation.BranchConditional -> {
                    ops.add(Instruction.CompareImmediate(ra = ams.environment[op.cond]!!, imm = 0, width = Width.Doubleword))
                    ops.add(Instruction.BranchIfEqual(target = Instruction.Label(op.falseTarget.name)))
                    ops.add(Instruction.BranchIfNotEqual(target = Instruction.Label(op.trueTarget.name)))
                }
                is IROperation.BranchAlways -> ops.add(Instruction.BranchAlways(target = Instruction.Label(op.target.name)))

                is IROperation.BooleanNeg -> ops.add(Instruction.XorImmediate(ams.allocateNewVar(op.rd), ams.environment[op.rs]!!, 1))
                is IROperation.IntegerNeg -> ops.add(Instruction.Neg(ams.allocateNewVar(op.rd), ams.environment[op.rs]!!))

                is IROperation.Phi -> throw IllegalStateException("Trying to lower phi op: $op")
            }
        }

    return LinearProgram(ops)
}
