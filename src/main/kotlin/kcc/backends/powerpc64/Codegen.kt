package kcc.backends.powerpc64

import kcc.backends.Backend
import kcc.backends.powerpc64.Instruction.*
import kcc.ir.IROperation
import kcc.ir.LinearProgram

internal sealed interface Storage

internal data class Stack(val slot: Int) : Storage // `slot` is merely an ID, it doesn't reflect the actual address

internal data class Register(val reg: StorageKind.Reg) : Storage

// This is a wrapper that will generate boilerplate code for shuttling to/from the stack, if necessary.
internal fun wrap(insn: (StorageKind.Reg) -> Instruction<StorageKind.Reg>, tempArea: Int, sd: Storage) : List<Instruction<StorageKind.Reg>> {
    val res = ArrayList<Instruction<StorageKind.Reg>>()
    val rd: StorageKind.Reg
    val d: Boolean

    when(sd) {
        is Register -> {
            d = false
            rd = sd.reg
        }
        is Stack -> {
            d = true
            rd = StorageKind.Reg.r28
            res += Store(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + tempArea) * 8).toShort(), Width.Doubleword)
        }
    }
    res += insn(rd)

    if (d) {
        res += Store(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + (sd as Stack).slot) * 8).toShort(), Width.Doubleword)
        res += Load(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + tempArea) * 8).toShort(), Width.Doubleword)
    }
    return res
}

internal fun wrap(insn: (StorageKind.Reg, StorageKind.Reg) -> Instruction<StorageKind.Reg>, tempArea: Int, sd: Storage, sa: Storage) : List<Instruction<StorageKind.Reg>> {
    val res = ArrayList<Instruction<StorageKind.Reg>>()
    val rd: StorageKind.Reg; val ra: StorageKind.Reg
    val d: Boolean; val a: Boolean

    when(sd) {
        is Register -> {
            d = false
            rd = sd.reg
        }
        is Stack -> {
            d = true
            rd = StorageKind.Reg.r28
            res += Store(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + tempArea) * 8).toShort(), Width.Doubleword)
        }
    }

    when(sa) {
        is Register -> {
            a = false
            ra = sa.reg
        }
        is Stack -> {
            a = true
            ra = StorageKind.Reg.r29
            res += Store(StorageKind.Reg.r29, StorageKind.Reg.r1, ((5 + tempArea) * 8).toShort(), Width.Doubleword)
            res += Load(StorageKind.Reg.r29, StorageKind.Reg.r1, ((4 + sa.slot) * 8).toShort(), Width.Doubleword)
        }
    }

    res += insn(rd, ra)

    if (d) {
        res += Store(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + (sd as Stack).slot) * 8).toShort(), Width.Doubleword)
        res += Load(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + tempArea) * 8).toShort(), Width.Doubleword)
    }
    if (a) res += Load(StorageKind.Reg.r29, StorageKind.Reg.r1, ((5 + tempArea) * 8).toShort(), Width.Doubleword)

    return res
}

internal fun wrap(insn: (StorageKind.Reg, StorageKind.Reg, StorageKind.Reg) -> Instruction<StorageKind.Reg>, tempArea: Int, sd: Storage, sa: Storage, sb: Storage) : List<Instruction<StorageKind.Reg>> {
    val res = ArrayList<Instruction<StorageKind.Reg>>()
    val rd: StorageKind.Reg; val ra: StorageKind.Reg; val rb: StorageKind.Reg
    val d: Boolean; val a: Boolean; val b: Boolean

    when(sd) {
        is Register -> {
            d = false
            rd = sd.reg
        }
        is Stack -> {
            d = true
            rd = StorageKind.Reg.r28
            res += Store(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + tempArea) * 8).toShort(), Width.Doubleword)
        }
    }

    when(sa) {
        is Register -> {
            a = false
            ra = sa.reg
        }
        is Stack -> {
            a = true
            ra = StorageKind.Reg.r29
            res += Store(StorageKind.Reg.r29, StorageKind.Reg.r1, ((5 + tempArea) * 8).toShort(), Width.Doubleword)
            res += Load(StorageKind.Reg.r29, StorageKind.Reg.r1, ((4 + sa.slot) * 8).toShort(), Width.Doubleword)
        }
    }

    when(sb) {
        is Register -> {
            b = false
            rb = sb.reg
        }
        is Stack -> {
            b = true
            rb = StorageKind.Reg.r30
            res += Store(StorageKind.Reg.r30, StorageKind.Reg.r1, ((6 + tempArea) * 8).toShort(), Width.Doubleword)
            res += Load(StorageKind.Reg.r30, StorageKind.Reg.r1, ((4 + sb.slot) * 8).toShort(), Width.Doubleword)
        }
    }

    res += insn(rd, ra, rb)

    if (d) {
        res += Store(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + (sd as Stack).slot) * 8).toShort(), Width.Doubleword)
        res += Load(StorageKind.Reg.r28, StorageKind.Reg.r1, ((4 + tempArea) * 8).toShort(), Width.Doubleword)
    }
    if (a) res += Load(StorageKind.Reg.r29, StorageKind.Reg.r1, ((5 + tempArea) * 8).toShort(), Width.Doubleword)
    if (b) res += Load(StorageKind.Reg.r30, StorageKind.Reg.r1, ((6 + tempArea) * 8).toShort(), Width.Doubleword)

    return res
}

internal fun ppc64AllocateRegister(p: LinearProgram<Instruction<StorageKind.Var>>): Triple<Map<StorageKind.Var, Storage>, Set<StorageKind.Reg>, Int> {
    // This is a pretty direct implementation of the original P&S' allocator.
    // ("Linear Scan Register Allocation", Poletto & Sarkar 1999)

    // The last SSA variable will be the return value.
    val returnVar = p.ops.last().defs().first()

    // This is a sorted set so prioritization happens automagically :)
    val freeRegisters = sortedSetOf(
        // First, allocate the volatile registers.
        StorageKind.Reg.r3, StorageKind.Reg.r4, StorageKind.Reg.r5, StorageKind.Reg.r6,
        StorageKind.Reg.r7, StorageKind.Reg.r8, StorageKind.Reg.r9, StorageKind.Reg.r10,

        // If we still need more space, then allocate the nonvolatiles too.
        StorageKind.Reg.r14, StorageKind.Reg.r15, StorageKind.Reg.r16, StorageKind.Reg.r17,
        StorageKind.Reg.r18, StorageKind.Reg.r19, StorageKind.Reg.r20, StorageKind.Reg.r21,
        StorageKind.Reg.r22, StorageKind.Reg.r23, StorageKind.Reg.r24, StorageKind.Reg.r25,
        StorageKind.Reg.r26, StorageKind.Reg.r27, StorageKind.Reg.r28, StorageKind.Reg.r29,
        StorageKind.Reg.r30
    )
    val occupiedRegisters = sortedSetOf<StorageKind.Reg>()
    val usedNonvolatiles = mutableSetOf<StorageKind.Reg>()

    val storageMap = HashMap<StorageKind.Var, Storage>(p.ops.size)
    storageMap[returnVar] = Register(StorageKind.Reg.r3) // PowerPC64 ABI requires return value to be placed in %r3.

    // Tracks every Var that's live at point i in the instruction stream.
    val lifetimes = ArrayList<MutableSet<StorageKind.Var>>(p.ops.size)
    val lastSeen = HashMap<StorageKind.Var, Int>(p.ops.size)

    // First, we do a pass through the entire program to collect the Vars' lifetimes.
    for ((i, insn) in p.ops.withIndex()) {
        val regs = insn.defs() + insn.uses()
        lifetimes.add(HashSet(regs.size))
        for (reg in regs) {
            when (lastSeen[reg]) {
                null -> {lifetimes[i].add(reg); lastSeen[reg] = i}
                else -> {for (j in lastSeen[reg]!!..i) lifetimes[j].add(reg); lastSeen[reg] = i}
            }
        }
    }

    // Second, using the collected data, we allocate the Vars to registers.
    var stackSlot = 0 // We need to track the position of the first available stack slot.
    for ((pos, vars) in lifetimes.withIndex()) {
        // This is intentionally split into two loops to simplify implementation,
        // at a cost of making it a tad slower.

        // First, we look for all live variables and assign storage to them.
        for (v in vars) {
            // If v is already assigned, skip the process.
            if (v in storageMap) continue
            try {
                // Take the first register available and assign it to v.
                val reg = freeRegisters.first()

                // If we picked a nonvolatile register, then take note of it.
                if (reg >= StorageKind.Reg.r14) {
                    usedNonvolatiles.add(reg)
                }

                storageMap[v] = Register(reg)
                occupiedRegisters.add(reg)
                freeRegisters.remove(reg)
            } catch (e: NoSuchElementException) {
                // We ran out of registers, need to spill a Var to the stack.
                // Find out the variable that's alive for longest and then spill that Var to the stack.
                val lastEnd = vars.map { Pair(it, lastSeen[it]) }.maxByOrNull { it.second!! }!!.first

                stackSlot++ // Bump the stack slot pointer.
                if (lastEnd in storageMap) {
                    val s = storageMap[lastEnd]!!
                    storageMap[v] = s
                    storageMap[lastEnd] = Stack(stackSlot)
                } else {
                    storageMap[v] = Stack(stackSlot)
                }
            }
        }

        // Second, we look if there's Vars whose lifetime is about to end and deallocate their backing storage.
        for (v in vars) {
            if (pos == lastSeen[v]) when (val s = storageMap[v]!!) {
                // If v is stored in a register, we can add back the register
                // into the free list and reuse it for another Var.
                is Register -> {
                    occupiedRegisters.remove(s.reg)
                    freeRegisters.add(s.reg)
                }

                // Or, if v is stored in the stack, do nothing.
                // This is suboptimal because we might use more stack space than is strictly necessary, but
                // I believe it's okay because the stack space is large enough that there's little chance
                // of using up all of it, and that PowerPC has a lot of temporary registers so in most programs
                // we wouldn't even need to do a spill anyway.
                is Stack -> {}
            }
        }
    }

    return Triple(storageMap, usedNonvolatiles, stackSlot)
}

internal fun ppc64Codegen(ir: LinearProgram<IROperation>): LinearProgram<Instruction<StorageKind.Reg>> {
    val ppcAbstractProgram = ppc64InstructionSelect(ir)
    val (storageMap, usedNonvolatiles, slotsUsed) = ppc64AllocateRegister(ppcAbstractProgram)
    val instructions = ArrayList<Instruction<StorageKind.Reg>>()

    // The general layout of our spill area would be:
    // (This figure omits other ABI fields and only shows the use of the Local Variable Space)
    // --------- High Address ---------
    // Padding (if needed, one slot)
    // The ABI requires the amount of
    // used slots to be an even number
    // (i.e multiples of 16 bytes).
    // --------------------------------
    // %r30 | Temporary area to save
    // %r29 | stack support registers
    // %r28 | (three slots)
    // --------------------------------
    // S(n) | Spilled variable area
    // ...  | (variable slots)
    // S(0) |
    // --------- Low  Address ---------
    // Nonvolatile register values is saved in the ABI-defined General Register Save Area field.
    // (see "Power Architecture 64-Bit ELF V2 ABI Specification: OpenPOWER ABI for Linux Supplement" version 1.1, IBM 2015)
    // Also, we will use %r28-%r30 as temporaries in case we need to move data to/from the spill area.

    // Compute the stack space to save our nonvolatiles and spills.
    // If slotsUsed is nonzero (i.e we are spilling) then add three more slots since there's
    // the possibility of using up tp three stack operands in an operation (in that case,
    // we need to borrow some register space and save their contents).
    // This is calculated in units of stack slots of 8 bytes each.

    val saveAreaStart = 4 + slotsUsed + if (slotsUsed > 0) 3 else 0
    val stackSpaceNeeded = usedNonvolatiles.size + saveAreaStart
    val padding = if (stackSpaceNeeded % 2 != 0) 1 else 0

    instructions += Label(".Lbb_prologue")

    // Before running our code, we need to save all the nonvolatiles to the stack
    // and allocate space for spilled Vars.
    instructions += MoveFromLR(StorageKind.Reg.r0)
    instructions += Store(StorageKind.Reg.r0, StorageKind.Reg.r1, 16, Width.Doubleword)
    instructions += StoreAndUpdate(StorageKind.Reg.r1, StorageKind.Reg.r1, ((stackSpaceNeeded + padding) * -8).toShort(), Width.Doubleword)
    for ((slot, r) in usedNonvolatiles.withIndex()) {
        instructions += Store(r, StorageKind.Reg.r1, ((saveAreaStart + padding + slot) * 8).toShort(), Width.Doubleword)
    }

    instructions += BranchAlways(target = Label(".Lbb_init"))

    // This is our program.
    // Now, we merge the result of isel and the allocation map from the register allocator,
    // while taking account for the variables that are allocated on-stack.
    for (i in ppcAbstractProgram.ops) {
        instructions += when(i) {
            is Add -> wrap(::Add, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is AddImmediate -> wrap({ rd, ra -> AddImmediate(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is AddImmediateShifted -> wrap({ rd, ra -> AddImmediateShifted(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is And -> wrap(::And, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is AndImmediate -> wrap({ rd, ra -> AndImmediate(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is AndImmediateShifted -> wrap({ rd, ra -> AndImmediateShifted(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is BranchAlways -> listOf(BranchAlways(Label(i.target.name)))
            is BranchIfEqual -> listOf(BranchIfEqual(i.cr, Label(i.target.name)))
            is BranchIfNotEqual -> listOf(BranchIfNotEqual(i.cr, Label(i.target.name)))
            is BranchToLR -> listOf(BranchToLR())
            is Compare -> wrap({ ra, rb -> Compare(i.cr, ra, rb, i.width) }, slotsUsed, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is CompareImmediate -> wrap({ ra -> CompareImmediate(i.cr, ra, i.imm, i.width) }, slotsUsed, storageMap[i.ra]!!)
            is CountLeadingZero -> wrap({ rd, rs -> CountLeadingZero(rd, rs, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.rs]!!)
            is Div -> wrap({ rd, ra, rb -> Div(rd, ra, rb, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is Label -> listOf(Label(i.name))
            is Load -> wrap({ rd, ps ->  Load(rd, ps, i.d, i.width)}, slotsUsed, storageMap[i.rd]!!, storageMap[i.ps]!!)
            is LoadAndUpdate -> wrap({ rd, ps ->  LoadAndUpdate(rd, ps, i.d, i.width)}, slotsUsed, storageMap[i.rd]!!, storageMap[i.ps]!!)
            is LoadImmediate -> wrap({ rd -> LoadImmediate(rd, i.imm) }, slotsUsed, storageMap[i.rd]!!)
            is LoadImmediateShifted -> wrap({ rd -> LoadImmediateShifted(rd, i.imm) }, slotsUsed, storageMap[i.rd]!!)
            is Move -> wrap({ rd, rs -> Move(rd, rs) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.rs]!!)
            is MoveFromLR -> wrap({ rd -> MoveFromLR(rd) }, slotsUsed, storageMap[i.rd]!!)
            is MoveToLR -> wrap({ rd -> MoveToLR(rd) }, slotsUsed, storageMap[i.rs]!!)
            is Mul -> wrap({ rd, ra, rb -> Mul(rd, ra, rb, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is MulH -> wrap({ rd, ra, rb -> MulH(rd, ra, rb, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is Neg -> wrap(::Neg, slotsUsed, storageMap[i.rd]!!, storageMap[i.rs]!!)
            is Or -> wrap(::Or, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is OrImmediate -> wrap({ rd, ra -> OrImmediate(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is OrImmediateShifted -> wrap({ rd, ra -> OrImmediateShifted(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is ShiftLeft -> wrap({ rd, ra, rb -> ShiftLeft(rd, ra, rb, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is ShiftLeftImmediate -> wrap({ rd, ra -> ShiftLeftImmediate(rd, ra, i.imm, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is ShiftRight -> wrap({ rd, ra, rb -> ShiftRight(rd, ra, rb, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is ShiftRightImmediate -> wrap({ rd, ra -> ShiftRightImmediate(rd, ra, i.imm, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is ShiftRightAlgebraic -> wrap({ rd, ra, rb -> ShiftRightAlgebraic(rd, ra, rb, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is ShiftRightAlgebraicImmediate -> wrap({ rd, ra -> ShiftRightAlgebraicImmediate(rd, ra, i.imm, i.width) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is Store -> wrap({ rs, pd ->  Store(rs, pd, i.d, i.width)}, slotsUsed, storageMap[i.rs]!!, storageMap[i.pd]!!)
            is StoreAndUpdate -> wrap({ rs, pd ->  StoreAndUpdate(rs, pd, i.d, i.width)}, slotsUsed, storageMap[i.rs]!!, storageMap[i.pd]!!)
            is Sub -> wrap(::Sub, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is SubImmediate -> wrap({ rd, ra -> SubImmediate(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is SubImmediateShifted -> wrap({ rd, ra -> SubImmediateShifted(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is Xor -> wrap(::Xor, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!, storageMap[i.rb]!!)
            is XorImmediate -> wrap({ rd, ra -> XorImmediate(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
            is XorImmediateShifted -> wrap({ rd, ra -> XorImmediateShifted(rd, ra, i.imm) }, slotsUsed, storageMap[i.rd]!!, storageMap[i.ra]!!)
        }
    }

    instructions += Label(".Lbb_epilogue")
    // After we're done, restore the nonvolatiles and deallocate the stack.
    for ((slot, r) in usedNonvolatiles.withIndex()) {
        instructions += Load(r, StorageKind.Reg.r1, ((saveAreaStart + padding + slot) * 8).toShort(), Width.Doubleword)
    }

    instructions += AddImmediate(StorageKind.Reg.r1, StorageKind.Reg.r1, ((stackSpaceNeeded + padding) * 8).toShort())
    instructions += Load(StorageKind.Reg.r0, StorageKind.Reg.r1, 16, Width.Doubleword)
    instructions += MoveToLR(StorageKind.Reg.r0)
    instructions += BranchToLR()

    return LinearProgram(instructions)
}

class PowerPC64Backend(private val ir: LinearProgram<IROperation>) : Backend {
    override fun toString(): String {
        return """.text
body:
${ppc64Codegen(ir)}

.global main
main:
addis %r2, %r12, .TOC.-main@ha
addi %r2, %r2, .TOC.-main@l
.localentry main, .-main
mflr %r0
std %r0, 16(%r1)
stdu %r1, -96(%r1)
bl body
mr %r4, %r3
addis %r3, %r2, str@toc@ha
addi %r3, %r3, str@toc@l
bl printf
nop
li %r3, 0
addi %r1, %r1, 96
ld %r0, 16(%r1)
mtlr %r0
blr

.rodata
str: .asciz "%ld\n"
"""
    }
}
