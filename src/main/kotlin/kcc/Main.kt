package kcc

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.check
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.enum
import kcc.backends.powerpc64.PowerPC64Backend
import kcc.ir.LinearProgram
import kcc.lang.parseLanguage
import java.io.File

enum class Backends {
    PowerPC64,

    // Those are for debugging intermediate representations
    AST,
    SSA,
    UnSSA,
    LinearIR,
}

class KCC : CliktCommand() {
    private val filename by argument(help = "File to compile").check { it.endsWith(".src") }
    private val target by option(
        "-target",
        metavar = "<target>",
        help = "Generate code for given target"
    ).enum<Backends>().default(Backends.SSA)
    private val output by option("-o", metavar = "<file>", help = "Output file name")

    override fun run() {
        val inFile = File(filename)
        val output = output ?: filename.replace(Regex("""\.src$"""), ".s")

        val ast = parseLanguage(inFile.readLines()).onFailure { throw it }.getOrNull()!!
        if (target == Backends.AST) {
            val outFile = File(output)
            outFile.writeText(ast.toString())
            return
        }

        val cfgIR = runCatching { ast.evaluate() }.onFailure { throw it }.getOrNull()!!
        if (target == Backends.SSA) {
            val outFile = File(output)
            outFile.writeText(cfgIR.toString())
            return
        }

        cfgIR.convertOutOfSSA()
        if (target == Backends.UnSSA) {
            val outFile = File(output)
            outFile.writeText(cfgIR.toString())
            return
        }

        val linearIR = LinearProgram(cfgIR)
        if (target == Backends.LinearIR) {
            val outFile = File(output)
            outFile.writeText(linearIR.toString())
            return
        }

        val powerPC64Program = PowerPC64Backend(linearIR)
        if (target == Backends.PowerPC64) {
            val outFile = File(output)
            outFile.writeText(powerPC64Program.toString())
            return
        }
    }
}

fun main(args: Array<String>) = KCC().main(args)
